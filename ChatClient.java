import java.io.*;
import java.net.*;
import java.time.*;
import java.text.*;
import java.util.*;
import javax.net.ssl.*;
import java.security.*;


public class ChatClient extends Thread
{
	protected int serverPort = 1234;
	protected static String private_certificate;
	protected static String private_password;

	public static void main(String[] args) throws Exception {
		if(args.length != 2){
			System.out.println("Podajte ime certifikata ob zagonu aplikacije: java ChatClient <imecertifikata>.private <koda>");
			System.exit(1);
		}
		private_certificate = args[0];
		private_password = args[1];
		new ChatClient();
	}

	public ChatClient() throws Exception {
		SSLSocket socket = null;
		DataInputStream in = null;
		DataOutputStream out = null;

		// connect to the chat server
		try {
			System.out.println("[system] connecting to chat server ...");
			/*
			socket = new Socket("localhost", serverPort); // create socket connection
			*/

			// preberi datoteko s streznikovim certifikatom
			KeyStore serverKeyStore = KeyStore.getInstance("JKS");
			serverKeyStore.load(new FileInputStream("server.public"), "public".toCharArray());
			
			// preberi datoteko s svojim certifikatom in tajnim kljucem
			KeyStore clientKeyStore = KeyStore.getInstance("JKS");
			clientKeyStore.load(new FileInputStream(private_certificate), private_password.toCharArray());
			
			// vzpostavi SSL kontekst (komu zaupamo, kaksni so moji tajni kljuci in certifikati)
			TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
			tmf.init(serverKeyStore);
			KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
			kmf.init(clientKeyStore, private_password.toCharArray());
			SSLContext sslContext = SSLContext.getInstance("TLS");
			SecureRandom scr = new SecureRandom();
			sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), scr);
			
			// kreiramo socket
			SSLSocketFactory sf = sslContext.getSocketFactory();
			socket = (SSLSocket) sf.createSocket("localhost", serverPort);
			socket.setEnabledCipherSuites(new String[] { "TLS_RSA_WITH_AES_128_CBC_SHA" }); // dovoljeni nacin kriptiranja (CipherSuite)
			socket.startHandshake(); // eksplicitno sprozi SSL Handshake


			in = new DataInputStream(socket.getInputStream()); // create input stream for listening for incoming messages
			out = new DataOutputStream(socket.getOutputStream()); // create output stream for sending messages
			System.out.println("[system] connected");

			ChatClientMessageReceiver message_receiver = new ChatClientMessageReceiver(in); // create a separate thread for listening to messages from the chat server
			message_receiver.start(); // run the new thread
			
		} catch (Exception e) {
			e.printStackTrace(System.err);
			System.exit(1);
		}

		// read from STDIN and send messages to the chat server
		BufferedReader std_in = new BufferedReader(new InputStreamReader(System.in));
		String userInput;

		
		
		System.out.println("\n NAVODILA ZA CERTIFIKAT:\n*Vsako ime uporabnika oz. certifikat na kanalu mora biti unikaten.* \n\n NAVODILA ZA ZASEBNO SPOROCILO: \n /zasebno \"<ime-naslovnika>\" <sporocilo> {brez < in >}\n\n");
		
		/*
		System.out.println("\nZapisite uporabnisko ime in pritisnite ENTER: ");
		*/

		while ((userInput = std_in.readLine()) != null) { // read a line from the console
			this.sendMessage(userInput, out); // send the message to the chat server
		}

		// cleanup
		out.close();
		in.close();
		std_in.close();
		socket.close();
	}

	private void sendMessage(String message, DataOutputStream out) {
		try {
			out.writeUTF(message); // send the message to the chat server
			out.flush(); // ensure the message has been sent
		} catch (IOException e) {
			System.err.println("[system] could not send message");
			e.printStackTrace(System.err);
		}
	}
}

// wait for messages from the chat server and print the out
class ChatClientMessageReceiver extends Thread {
	private DataInputStream in;

	public String getCurrentLocalDateTimeStamp() {
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}

	public ChatClientMessageReceiver(DataInputStream in) {
		this.in = in;
	}

	public void run() {
		try {
			String message;
			while ((message = this.in.readUTF()) != null) { // read new message
				if(Objects.equals(message, "CERTIFICATE DUPLICATE")) {
					System.out.println("[WARNING] Registracija neuspesna. Uporabili ste certifikat, s katerim se je nekdo ze prijavil v klepetalnico, prosim ponovno zazenite program in uporabite kaksen drug certifikat\n");
					System.exit(1);
				}
				System.out.println("["+getCurrentLocalDateTimeStamp()+"] [RKchat] " + message); // print the message to the console
			}
		} catch (Exception e) {
			System.err.println("[system] could not read message");
			e.printStackTrace(System.err);
			System.exit(1);
		}
	}
}
