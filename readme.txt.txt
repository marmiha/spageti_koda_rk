 ZAGON KLEPETALNICE: 
 Podajte ime certifikata ob zagonu aplikacije: java ChatClient <ime_certifikata>.private <koda_certifkata> {brez < in >}

 primer:
 java ChatClient babica.private babicapwd

 NAVODILA ZA CERTIFIKAT:
 *Vsako ime uporabnika oz. certifikat na kanalu mora biti unikaten.*

 NAVODILA ZA ZASEBNO SPOROCILO:
 /zasebno "<ime-naslovnika>" <sporocilo> {brez < in >}