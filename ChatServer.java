import java.io.*;
import java.net.*;
import java.util.*;
import java.time.*;
import java.text.*;
import javax.net.ssl.*;
import java.security.*;

public class ChatServer {

	protected int serverPort = 1234;
	protected List<Socket> clients = new ArrayList<Socket>(); // list of clients
	protected List<SocketIme> clientNames = new ArrayList<SocketIme>();

	public static void main(String[] args) throws Exception {
		new ChatServer();
	}

	public ChatServer() {
		SSLServerSocket serverSocket = null;

		// create socket
		try {
			String passphrase = "serverpwd";

			// preberi datoteko z odjemalskimi certifikati
			KeyStore clientKeyStore = KeyStore.getInstance("JKS"); // KeyStore za shranjevanje odjemalcevih javnih kljucev (certifikatov)
			clientKeyStore.load(new FileInputStream("client.public"), "public".toCharArray());

			// preberi datoteko s svojim certifikatom in tajnim kljucem
			KeyStore serverKeyStore = KeyStore.getInstance("JKS"); // KeyStore za shranjevanje strežnikovega tajnega in javnega kljuca
			serverKeyStore.load(new FileInputStream("server.private"), passphrase.toCharArray());

			// vzpostavi SSL kontekst (komu zaupamo, kakšni so moji tajni kljuci in certifikati)
			TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
			tmf.init(clientKeyStore);
			KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
			kmf.init(serverKeyStore, passphrase.toCharArray());
			SSLContext sslContext = SSLContext.getInstance("TLS");
			SecureRandom scr = new SecureRandom();
			sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), scr);

			// kreiramo socket
			SSLServerSocketFactory factory = sslContext.getServerSocketFactory();
			serverSocket= (SSLServerSocket) factory.createServerSocket(this.serverPort);
			serverSocket.setNeedClientAuth(true); // tudi odjemalec se MORA predstaviti s certifikatom
			serverSocket.setEnabledCipherSuites(new String[] {"TLS_RSA_WITH_AES_128_CBC_SHA"});

		} catch (Exception e) {
			System.err.println("["+getCurrentLocalDateTimeStamp()+"] "+"[system] could not create socket on port " + this.serverPort);
			e.printStackTrace(System.err);
			System.exit(1);
		}

		// start listening for new connections
		System.out.println("["+getCurrentLocalDateTimeStamp()+"] "+"[system] listening ...");
		try {
			while (true) {
				//Socket newClientSocket = serverSocket.accept(); // wait for a new client connection
				Socket newClientSocket = serverSocket.accept(); // vzpostavljena povezava
				((SSLSocket)newClientSocket).startHandshake(); // eksplicitno sprozi SSL Handshake
				String username = ((SSLSocket) newClientSocket).getSession().getPeerPrincipal().getName();
				String[] split = username.split("=");
				username = split[1];
				System.out.println("Established SSL connection with: " + username);
				
				synchronized(this) {
					clients.add(newClientSocket); // add client to the list of clients
				}

				ChatServerConnector conn = new ChatServerConnector(this, newClientSocket, username); // create a new thread for communication with the new client
				conn.start(); // run the new thread
			}
		} catch (Exception e) {
			System.err.println("["+getCurrentLocalDateTimeStamp()+"] "+"[error] Accept failed.");
			e.printStackTrace(System.err);
			System.exit(1);
		}

		// close socket
		System.out.println("["+getCurrentLocalDateTimeStamp()+"] "+"[system] closing server socket ...");
		try {
			serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace(System.err);
			System.exit(1);
		}
	}

	// send a message to all clients connected to the server
	public void sendToAllClients(String message) throws Exception {
		Iterator<Socket> i = clients.iterator();
		while (i.hasNext()) { // iterate through the client list
			Socket socket = (Socket) i.next(); // get the socket for communicating with this client
			try {
				DataOutputStream out = new DataOutputStream(socket.getOutputStream()); // create output stream for sending messages to the client
				out.writeUTF(message); // send message to the client
			} catch (Exception e) {
				System.err.println("["+getCurrentLocalDateTimeStamp()+"] "+"[system] could not send message to a client");
				e.printStackTrace(System.err);
			}
		}
	}

	public boolean prostoIme(String ime){
		Iterator<SocketIme> i = clientNames.iterator();
		SocketIme socketIme = null;
		int port = 0;
		String ip = null;
		while(i.hasNext()){

			ime = ime.toUpperCase();
			socketIme = (SocketIme) i.next();
			port = socketIme.socket.getPort();
			ip = socketIme.socket.getInetAddress().getHostAddress();
			String uporabljenoIme = socketIme.ime.toUpperCase();
			if(ime.equals(uporabljenoIme)){
				System.out.println("["+getCurrentLocalDateTimeStamp()+"] "+"[System] ["+ ip + "] Novi uporbnik je uporabil certifikat z imenom '"+ ime + "', ki je ze v uporabi, disconnected.");
				return false;
			}
		}
		if(port != 0 ) System.out.println("["+getCurrentLocalDateTimeStamp()+"] "+"[System] ["+ip+":"+port+"] Novi uporabnik si je dodelil ime: " + ime);
		return true;
	}

	public boolean sendToOneClient(String naslovnik, String msg, String ime){
		Iterator<SocketIme> i = clientNames.iterator();
		SocketIme socketIme = null;
		while(i.hasNext()){
			naslovnik = naslovnik.toUpperCase();
			socketIme = (SocketIme) i.next();
			String uporabljenoIme = socketIme.ime.toUpperCase();
			if(naslovnik.equals(uporabljenoIme)){
				try {
					DataOutputStream out = new DataOutputStream(socketIme.socket.getOutputStream()); // create output stream for sending messages to the client
					String msg_rdy = "[ZASEBNO] od "+ ime +": "+ msg;
					out.writeUTF(msg_rdy);
					return true;
				} catch (Exception e) {
					System.err.println("["+getCurrentLocalDateTimeStamp()+"] "+"[system] could not send message to a client");
					e.printStackTrace(System.err);
				}
			}
		}
		return false;
	}

	public String getCurrentLocalDateTimeStamp() {
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}

	public void addSocketIme(SocketIme SM){
		synchronized(this){
			clientNames.add(SM);
		}
	}

	public void removeSocketIme(SocketIme SM){
		synchronized(this) {
			clientNames.remove(SM);
		}
	}

	public void removeClient(Socket socket) {
		synchronized(this) {
			clients.remove(socket);
		}
	}
}

class SocketIme {
	public Socket socket;
	public String ime;

	public SocketIme (Socket socket, String ime){
		this.socket = socket;
		this.ime = ime;
	}
}

class ChatServerConnector extends Thread {
	private ChatServer server;
	private Socket socket;
	private SocketIme socketIme;
	

	public ChatServerConnector(ChatServer server, Socket socket, String ime) {
		this.server = server;
		this.socket = socket;
		if(server.prostoIme(ime)){
			this.socketIme = new SocketIme(socket, ime);
			this.server.addSocketIme(this.socketIme);}
		else{
			try {
				DataOutputStream out = new DataOutputStream(this.socket.getOutputStream()); // create output stream for sending messages to the client
				out.writeUTF("CERTIFICATE DUPLICATE");
			} catch (Exception e) {
				System.err.println("["+getCurrentLocalDateTimeStamp()+"] "+"[system] could not send message to a client");
				e.printStackTrace(System.err);
			}
			this.server.removeClient(socket);
		}
	}

	public String getCurrentLocalDateTimeStamp() {
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}

	public void run() {
		boolean kicked = false;
		if(this.socketIme == null) kicked = true;

		if(!kicked){System.out.println("["+getCurrentLocalDateTimeStamp()+"] "+"[system] connected with " + this.socket.getInetAddress().getHostName() + ":" + this.socket.getPort());
		System.err.println("["+getCurrentLocalDateTimeStamp()+"] "+"[system] "+ this.socket.getInetAddress().getHostName()+ ":" +  this.socket.getPort()  + " ima vzdevek: " + this.socketIme.ime);
		}
		DataInputStream in;
		try {
			in = new DataInputStream(this.socket.getInputStream()); // create input stream for listening for incoming messages
		} catch (IOException e) {
			System.err.println("["+getCurrentLocalDateTimeStamp()+"] [system] could not open input stream!");
			e.printStackTrace(System.err);
			this.server.removeClient(socket);
			return;
		}
		/*
		boolean hasName = false;
		while(!hasName){
			String ime;
			try {
				synchronized(this){
					ime = in.readUTF(); // read name from client
					DataOutputStream out = new DataOutputStream(this.socket.getOutputStream()); // create output stream for sending messages to the client
					if(!ime.matches("[a-zA-Z0-9]+") || ime.length() < 2) {
						out.writeUTF("Ime sme vsebovati samo crke in stevilke ter mora biti vsaj dolzine 2, vipisite kasen drugacen vzdevek in pritisnite ENTER:"); 
					}
					else if(this.server.prostoIme(ime)){
						this.socketIme = new SocketIme(this.socket, ime);
						this.server.addSocketIme(this.socketIme);
						hasName = true;
						out.writeUTF("Ime '" + ime +"' je bilo uspesno dodeljeno. Lahko pricnete s klepetanjem!");
						System.err.println("["+getCurrentLocalDateTimeStamp()+"] "+"[system] "+ this.socket.getInetAddress().getHostName()+ ":" +  this.socket.getPort()  + " si je dodelil vzdevek: "+ ime);
					} else{
						out.writeUTF("Ime '"+ ime + "' je ze zasedeno, vipisite kasen drugacen vzdevek in pritisnite ENTER:"); // send to the requesting client
					}
				}
			} catch (Exception e) {
				System.err.println("["+getCurrentLocalDateTimeStamp()+"] "+"[system] there was a problem while reading message client on port " + this.socket.getPort() + ", removing client");
				e.printStackTrace(System.err);
				this.server.removeClient(this.socket);
				return;
			}
		}
		*/

		while (true && !kicked) { // infinite loop in which this thread waits for incoming messages and processes them
			String msg_received;
			try {
				msg_received = in.readUTF(); // read the message from the client
			} catch (Exception e) {
				System.err.println("["+getCurrentLocalDateTimeStamp()+"] "+"[system] there was a problem while reading message client on port " + this.socket.getPort() + ", removing client with name: "+ this.socketIme.ime);
				e.printStackTrace(System.err);
				this.server.removeClient(this.socket);
				this.server.removeSocketIme(this.socketIme);
				return;
			}

			if (msg_received.length() == 0) // invalid message
				continue;

			System.out.println("["+getCurrentLocalDateTimeStamp()+"] "+"[RKchat] [" + this.socket.getPort() + "] ["+ this.socketIme.ime +"]: " + msg_received); // print the incoming message in the console

			String msg_send = this.socketIme.ime+" said: " + msg_received.toUpperCase(); // TODO


			try {
				if(msg_received.startsWith("/zasebno ")){
					if(msg_received.split(" ").length < 3){
						try {
							DataOutputStream out = new DataOutputStream(this.socket.getOutputStream()); // create output stream for sending messages to the client
							out.writeUTF("Napacna uporaba ukaza /zasebno");
						} catch (Exception e) {
							System.err.println("["+getCurrentLocalDateTimeStamp()+"] "+"[system] could not send message to a client");
							e.printStackTrace(System.err);
						}
					}
					msg_received = msg_received.split("/zasebno ")[1];
					
					String naslovnik = msg_received.split("\"")[1];
					msg_received = msg_received.split("\"")[2];
					System.out.println("["+getCurrentLocalDateTimeStamp()+"] "+"[ZASEBNO][ZA: "+naslovnik+"] [SPOROCILO: "+msg_received+"]");
					if (!this.server.sendToOneClient(naslovnik, msg_received, this.socketIme.ime)){
						try {
							DataOutputStream out = new DataOutputStream(this.socket.getOutputStream()); // create output stream for sending messages to the client
							out.writeUTF("Uporabnik, ki ste mu zeleli poslati sporocilo ni dosegljiv.");
						} catch (Exception e) {
							System.err.println("["+getCurrentLocalDateTimeStamp()+"] "+"[system] could not send message to a client");
							e.printStackTrace(System.err);
						}
					}
				}else{
					this.server.sendToAllClients(msg_send); // send message to all clients
				}
			} catch (Exception e) {
				System.err.println("["+getCurrentLocalDateTimeStamp()+"] "+"[system] there was a problem while sending the message to all clients or there was an incorrect input for /zasebno");
				e.printStackTrace(System.err);
				continue;
			}
		}
	}
}
